import requests
import io
import pandas as pd
import time
import re
from import_postgre import psycopg2_conn, creation_engine
import geopandas as gpd
import os
from shapely import wkt

couches_base = [
    ['wfs_du:doc_urba','brut.docs_urba_wfs'],
    #['wfs_du:doc_urba_com','brut.docs_urba_com_wfs'],
    ['wfs_du:document','brut.docs_urba_id_wfs'],
    ['wfs_scot:doc_urba','brut.scots_wfs'],
    #['wfs_scot:doc_urba_com','brut.scots_com_wfs'],
    ['wfs_scot:scot','brut.scots_id_wfs'],
    #['wfs_scot:perimetre_scot','brut.scots_perimetre_wfs']
]

couches_base_ign = [
    ['ADMINEXPRESS-COG.LATEST:commune', 'brut.communes_ign'],
    # ['ADMINEXPRESS-COG.LATEST:region', 'brut.regions_ign'],
    # ['ADMINEXPRESS-COG.LATEST:epci', 'brut.epcis_ign'],
    # ['ADMINEXPRESS-COG.LATEST:departement', 'brut.departements_ign']
]

# TODO : faire en sorte que cette fonction permette de gérer les données géographiques (comme pour l'ign) (il ne semble cependant pas vraiment y avoir de nécessité de cette fonctionnalité pour l'instant)
def fetch_all_data(couche):
    """Récupère toutes les données venant d'une couche du flux wfs du géoportail de l'urbanisme

    Args:
        couche (str): nom de la couche

    Returns:
        Pandas DataFrame: DataFrame cotenant toutes les données de la couche
    """
    layer_name = couche
    wfs_csv_url = "https://wxs-gpu.mongeoportail.ign.fr/externe/39wtxmgtn23okfbbs1al2lz3/wfs"
    
    all_data = []  # Pour stocker toutes les données

    page = 1
    while True:
        if page == 1:
            params = {
                "request": "GetFeature",
                "typeName": layer_name,
                "outputFormat": "csv",
                "startIndex": str(0),  # premier élément
                "count": "10000"  # Le nombre d'objet que l'on peut extraire en une requète est donné par le géoportail de l'urbanisme
            }
        else:
            params = {
                "request": "GetFeature",
                "typeName": layer_name,
                "outputFormat": "csv",
                "startIndex": str((page - 1) * 10000),  # index de la première ligne récupérée par la requète
                "count": "10000"   # Le nombre d'objet que l'on peut extraire en une requète est donné par le géoportail de l'urbanisme
            }
        response = requests.get(wfs_csv_url, params=params)
        csv_content = response.text

        # Charger les données CSV dans un DataFrame pandas
        df = pd.read_csv(io.StringIO(csv_content),dtype='str')
        # Ajouter les données de cette page au total
        all_data.append(df)
        
        # Si le nombre de lignes lues est inférieur à la taille de la page, cela signifie que c'est la dernière page
        if len(df) < 10000:
            break
        
        page += 1

    # Concaténer toutes les données en un seul DataFrame
    final_df = pd.concat(all_data, ignore_index=True)
    return final_df


def fetch_all_data_ign(couche):
    """Récupère toutes les données d'une couche du flux xfs de l'ign (y compris les données géographiques)

    Args:
        couche (str): nom de la couche

    Returns:
        Pandas DataFrame: dataframe contenant toutes les données
    """    
    layer_name = couche
    wfs_csv_url = "https://data.geopf.fr/wfs/ows?SERVICE=WFS&VERSION=2.0.0"
    
    all_data = []  # Pour stocker toutes les données

    page = 1
    while True:
        if page == 1:
            params = {
                "request": "GetFeature",
                "typeName": layer_name,
                "outputFormat": "json",
                "startIndex": str(0),  # premier élément
                "count": "1000"  # Le nombre d'objet que l'on peut extraire en une requète est donné par l'ign
            }
        else:
            params = {
                "request": "GetFeature",
                "typeName": layer_name,
                "outputFormat": "json",
                "startIndex": str((page - 1) * 1000),  # index de la première ligne récupérée par la requète
                "count": "1000"  # Le nombre d'objet que l'on peut extraire en une requète est donné par l'ign
            }
        response = requests.get(wfs_csv_url, params=params)
        with open('import_donnees/stockage_fichier_temp/test.json','wb') as file:
            file.write(response.content)
        # Charger les données CSV dans un DataFrame pandas
        df = gpd.read_file('import_donnees/stockage_fichier_temp/test.json')
        os.remove('import_donnees/stockage_fichier_temp/test.json')
        df['geometry'] = df['geometry'].apply(wkt.dumps)
        # Ajouter les données de cette page au total
        all_data.append(df)
        
        # Si le nombre de lignes lues est inférieur à la taille de la page, cela signifie que c'est la dernière page
        if len(df) < 1000:
            break
        
        page += 1

    # Concaténer toutes les données en un seul DataFrame
    final_df = pd.concat(all_data, ignore_index=True)
    return final_df


def import_base_postgre():
    """Importe les données du flux wfs du géoportail de l'urbanisme dans la base de données postgresql
    """    
    for x in couches_base:
        t = x[1].split('.')
        conn = psycopg2_conn()
        cursor = conn.cursor()
        requete1 = "DELETE FROM " + x[1] + ";"
        requete2 =  f"SELECT column_name FROM information_schema.columns WHERE table_schema = '{t[0]}' AND table_name = '{t[1]}' ;"
        cursor.execute(requete1)
        conn.commit()
        cursor.execute(requete2)
        infos = cursor.fetchall()
        conn.close()
        cursor.close()
        colonnes = [y[0] for y in infos]
        df = fetch_all_data(x[0])
        df.columns = colonnes
        engine = creation_engine()
        df.to_sql(t[1], engine, t[0], if_exists="append", index=False)
    

def import_base_postgre_ign():
    """Importe les données du flux wfs de l'ign dans la base postgresql
    """    
    for x in couches_base_ign:
        t = x[1].split('.')
        conn = psycopg2_conn()
        cursor = conn.cursor()
        requete1 = "DELETE FROM " + x[1] + ";"
        requete2 =  f"SELECT column_name FROM information_schema.columns WHERE table_schema = '{t[0]}' AND table_name = '{t[1]}' ;"
        cursor.execute(requete1)
        conn.commit()
        cursor.execute(requete2)
        infos = cursor.fetchall()
        conn.close()
        cursor.close()
        colonnes = [y[0] for y in infos]
        df = fetch_all_data_ign(x[0])
        df.columns = colonnes
        engine = creation_engine()
        df.to_sql(t[1], engine, t[0], if_exists="append", index=False)
    