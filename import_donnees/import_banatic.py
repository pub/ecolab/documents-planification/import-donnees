import pandas as pd
from import_postgre import import_stock, path, creation_engine, psycopg2_conn
import csv
import codecs
import os

def conversion_utf8(file, destination):
    """Converti un fichier au format utf-8

    Args:
        file (str): chemin vers le fichier
        destination (str): chemin vers le répertoire du fichier à créer
    """    
    # Spécifiez le chemin du fichier d'entrée ANSI
    fichier_ansi = file

    # Spécifiez le chemin du fichier de sortie UTF-8
    fichier_utf8 = destination + 'test.csv'

    # Ouvrez le fichier d'entrée en mode lecture avec le codec ANSI
    with codecs.open(fichier_ansi, 'r', encoding='iso-8859-1') as fichier_entree:
        # Lisez le contenu du fichier d'entrée
        contenu = fichier_entree.read()

    # Ouvrez le fichier de sortie en mode écriture avec le codec UTF-8
    with codecs.open(fichier_utf8, 'x', encoding='utf-8') as fichier_sortie:
        # Écrivez le contenu converti dans le fichier de sortie
        fichier_sortie.write(contenu)
    os.remove(file)
    os.rename(fichier_utf8, destination)


annees = ["2023", "2022", "2021", "2020", "2019", "2018", "2017", "2016", "2015", "2014", "2013", "2012", "2011", "2010"]

donnees_annees = []
for annee in annees:
    """Récupération des données des collectivités du banatic pour toutes les années et pour toutes les régions (le banatic donne des données par région et non des données concaténées)
    """    
    if int(annee) > 2015:
        urls = [
            f"telecharger.php?zone=R84&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R27&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R53&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R24&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R94&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R44&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R01&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R03&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R32&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R11&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R04&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R02&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R28&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R75&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R76&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R52&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R93&date=01/01/{annee}&format=D"
        ]
    else:
        urls = [
            f"telecharger.php?zone=R42&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R72&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R83&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R25&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R26&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R53&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R24&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R21&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R94&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R43&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R01&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R03&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R23&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R11&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R91&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R04&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R74&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R41&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R02&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R73&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R31&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R52&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R22&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R54&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R93&date=01/01/{annee}&format=D",
            f"telecharger.php?zone=R82&date=01/01/{annee}&format=D"
        ]
    all_data = []
    for url in urls:
        import_stock('https://www.banatic.interieur.gouv.fr/V5/fichiers-en-telechargement/' + url,'.csv')
        chemin = path + "/test" + '.csv'
        conversion_utf8(chemin, chemin)
        try:
            with open(chemin, "r") as csvfile:
                dialect = csv.Sniffer().sniff(csvfile.read(1024))
                csvfile.seek(0)
                df = pd.read_csv(chemin, sep=dialect.delimiter, quotechar='"',dtype='str')
                df['Annee'] = annee
                if 'Adhésion siren' not in df.index:
                    df['Adhésion siren'] = ''
                df = df[['N° SIREN', 'Nom du groupement', 'Type', 'Siren membre', 'Adhésion siren', 'Annee']]
        except:
            with open(chemin, "r") as csvfile:
                dialect = csv.Sniffer().sniff(csvfile.read(100))
                csvfile.seek(0)
                df = pd.read_csv(chemin, sep=dialect.delimiter, quotechar='"',dtype='str')
                df['Annee'] = annee
                if 'Adhésion siren' not in df.index:
                    df['Adhésion siren'] = ''
                df = df[['N° SIREN', 'Nom du groupement', 'Type', 'Siren membre', 'Adhésion siren', 'Annee']]
        os.remove(chemin)
        all_data.append(df)
    concat_df = pd.concat(all_data, ignore_index=True)
    donnees_annees.append(concat_df)
final_df = pd.concat(donnees_annees, ignore_index=True)

# Ajout des données dans la base postgresql
engine = creation_engine()
conn = psycopg2_conn()
cursor = conn.cursor()
requete1 = "DELETE FROM brut.collectivites_annees;"
requete2 = "SELECT column_name FROM information_schema.columns WHERE table_schema = 'brut' AND table_name = 'collectivites_annees' ;"
cursor.execute(requete1)
conn.commit()
cursor.execute(requete2)
infos = cursor.fetchall()
cursor.close()
conn.close()
colonnes = [x[0] for x in infos]
final_df.columns = colonnes
final_df.to_sql('collectivites_annees', engine, 'brut', if_exists="append", index=False)
