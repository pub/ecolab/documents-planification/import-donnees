from owslib.wfs import WebFeatureService
import shutil
x=1
# URL du service WFS
url_wfs = "https://wxs.ign.fr/administratif/geoportail/wfs"
url_wfs = "https://wxs.ign.fr/administratif/geoportail/wfs?SERVICE=WFS&VERSION=2.0.0&REQUEST=GetCapabilities"

# Se connecter au service WFS
wfs = WebFeatureService(url_wfs)
wfs11 = wfs
operations = wfs.operations
from owslib.wfs import WebFeatureService

# Remplacez l'URL par l'URL de votre propre service WFS
wfs_url = "https://wxs-gpu.mongeoportail.ign.fr/externe/39wtxmgtn23okfbbs1al2lz3/wfs"

wfs = WebFeatureService(wfs_url)
test = wfs.getfeature(typename='ADMINEXPRESS-COG.LATEST:commune', outputFormat='json',propertyname='*')
with open('/home/onyxia/work/docs-planification-import/import_donnees/stockage_fichier_temp/test', 'wb') as output_file:
    shutil.copyfileobj(test, output_file)

test.read()

# Obtenez la liste des opérations disponibles
operations = wfs.operations

# Parcourez la liste des opérations et affichez les informations
for operation in operations:
    print(f"Nom de l'opération : {operation.name}")
    print(f"Titre de l'opération : {operation.title}")
    print("\n")

# Obtenir la liste des couches disponibles
liste_couches = list(wfs.contents)

# Afficher la liste des couches
for couche in liste_couches:
    description_couche = wfs.get_schema(couche)
    if 'gpu_id' in description_couche['properties'].keys():
        print(couche)
description_couche = wfs.get_schema('ADMINEXPRESS-COG.LATEST:departement')

for couche in liste_couches:
    description_couche = wfs.get_schema(couche)
    print(couche)
    print(description_couche['properties'].keys())

fetch_all_data('wfs_du:municipality').head()

pd.set_option('display.max_columns', None)
fetch_all_data('wfs_du:doc_urba').head()

wfs.get_schema('wfs_du:municipality')


import re

# Chaîne WKT d'exemple
wkt = "MULTIPOLYGON (((46.92274755446327 4.427393252325408, 46.92262437674515 4.4274504623603175, 46.922378877559986 4.427641136542509, 46.92228648876026 4.427030155310113, 46.92221535534323 4.426593230355493, 46.92202133693878 4.426408048951994, 46.92188311524207 4.426225654321094, 46.92177578877119 4.4261663082253335, 46.9215889344676 4.426129838045173, 46.921442150760114 4.426129911308336, 46.920930129797995 4.426172928339183)))"

# Utilisez une expression régulière pour extraire les coordonnées et les inverser
pattern = r"(\d+\.\d+) (\d+\.\d+)"
wkt_inverse = re.sub(pattern, r"\2 \1", wkt)

# Affichez la chaîne WKT inversée
print(wkt_inverse)
