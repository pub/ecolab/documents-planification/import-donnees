import requests
import os
import pandas as pd
import psycopg2
from io import StringIO 
from sqlalchemy import create_engine
import csv
import zipfile

path = 'import_donnees/stockage_fichier_temp'

# Liste des tables à importer (hors zip)
liste_url_types_tables = [
    ['https://www.collectivites-locales.gouv.fr/files/Accueil/DESL/2023/epcicom2023.xlsx', '.xlsx', 'brut.com_epci_2023'],
    ['https://www.data.gouv.fr/fr/datasets/r/8ab9eb1e-1d19-4b3f-8a60-05a3399ab8d2', '.xlsx', 'brut.collectivites_all'],
    ['https://www.insee.fr/fr/statistiques/fichier/6800675/v_departement_2023.csv', '.csv', 'brut.dep'],
    ['https://www.gesteau.fr/sites/default/files/export/sage_necessaire_sdage.csv', '.csv', 'brut.sage_sdage'],
    ['https://www.gesteau.fr/sites/default/files/regle_sage/regles_sage.csv?regles_sage', '.csv', 'brut.regles_sages'],
    ['https://www.gesteau.fr/sites/default/files/export/sage_export_comite.csv', '.csv', 'brut.liste_sages'],
    ['https://www.gesteau.fr/sites/default/files/export/export_sage_site_web.csv', '.csv', 'brut.lien_sages'],
    ['https://www.gesteau.fr/sites/default/files/export/export_sage_dates_etat.csv', '.csv', 'brut.dates_sages'],
    #['https://www.data.gouv.fr/fr/datasets/r/fb3580f6-e875-408d-809a-ad22fc418581', '.json', 'brut.communes_contours'],
    ['https://www.data.gouv.fr/fr/datasets/r/6df10001-8ca5-4da2-a478-475b0ce12977', '.csv', 'brut.com_crte'],
    ['https://services.sandre.eaufrance.fr/telechargement/geo/COM/Commune/2023/Commune2023.csv', '.csv', 'brut.communes_sdages'],
    ['https://services.sandre.eaufrance.fr/telechargement/geo/COM/CircAdminBassin/2023/CircAdminBassin2023.csv', '.csv', 'brut.nom_sdages'],
    ['https://www.insee.fr/fr/statistiques/fichier/6800675/v_region_2023.csv', '.csv', 'brut.reg'],
    ['https://www.territoires-climat.ademe.fr/download/50/ressource-document', '.xlsx', 'brut.pcaet_dgec_ademe'],
    ['https://www.territoires-climat.ademe.fr/opendata/Demarches_PCAET_V1_entete.csv', '.csv', 'brut.demarches_pcaet_v1'],
    ['https://www.territoires-climat.ademe.fr/opendata/Demarches_PCAET_V2_entete.csv', '.csv', 'brut.demarches_pcaet_v2'],
    ['https://www.territoires-climat.ademe.fr/opendata/Correspondance_demarche_fichiers.csv', '.csv', 'brut.pcaet_demarches_fichiers'],
    #['https://www.territoires-climat.ademe.fr/opendata/Demarches_PCAET_V1_pec_seq.csv', '.csv', 'brut.demarches_pcaet_pec_sec_v1'],
    #['https://www.territoires-climat.ademe.fr/opendata/Demarches_PCAET_V2_pec_seq.csv', '.csv', 'brut.demarches_pcaet_pec_sec_v2'],
    #['https://www.territoires-climat.ademe.fr/opendata/Demarches_PCAET_V1_enr.csv', '.csv', 'brut.demarches_pcaet_enr_v1'],
    #['https://www.territoires-climat.ademe.fr/opendata/Demarches_PCAET_V2_enr.csv', '.csv', 'brut.demarches_pcaet_enr_v2'],
    #['https://www.territoires-climat.ademe.fr/opendata/Demarches_PCAET_V1_polluant.csv', '.csv', 'brut.demarches_pcaet_polluant_v1'],
    #['https://www.territoires-climat.ademe.fr/opendata/Demarches_PCAET_V2_polluant.csv', '.csv', 'brut.demarches_pcaet_polluant_v2'],
    ['https://plans-mobilite.cerema.fr/mobility_plans/download/plans_list_with_filters?pdm_type%5B38%5D=38&pdm_type%5B39%5D=39&pdm_type%5B40%5D=40&pdm_type%5B41%5D=41&pdm_type%5B42%5D=42&pdm_type%5B43%5D=43&pdm_type%5B44%5D=44&pdm_type%5B45%5D=45&pdm_type%5B46%5D=46', '.csv', 'brut.pdm'],
    ['https://plans-mobilite.cerema.fr/mobility_plans/download/territorial_covered_by_plan?destination=/chiffres-cles', '.csv', 'brut.com_pdm']
]

# Liste des tables venant de fichier zip à importer
liste_nom_doc_zip = [['https://www.gesteau.fr/sites/default/files/export/sage_administratif_csv.zip','.csv','brut.com_sages','sage_communes.csv'],
    ['https://www.insee.fr/fr/statistiques/fichier/2521169/base_cc_comparateur_csv.zip','.csv','brut.base_cc_comparateur','base_cc_comparateur.csv'],
    ['https://www.insee.fr/fr/statistiques/fichier/2028028/table_passage_annuelle_2023.zip','.xlsx','brut.passage_2023','table_passage_annuelle_2023.csv'],
    ['https://www.data.gouv.fr/fr/datasets/r/42b16d68-958e-4518-8551-93e095fe8fda','.xlsx','brut.communes_siren','Banatic_SirenInsee2023.xlsx'],
    ['https://www.banatic.interieur.gouv.fr/V5/ressources/documents/document_reference/TableCorrespondanceSirenInsee.zip', '.xlsx', 'brut.communes_siren_2023', 'Banatic_SirenInsee2023.xlsx'],
    ['https://www.banatic.interieur.gouv.fr/V5/ressources/documents/document_reference/TableCorrespondanceSirenInsee.zip', '.xlsx', 'brut.communes_siren_2022', 'Banatic_SirenInsee2022.xlsx'],
    ['https://www.banatic.interieur.gouv.fr/V5/ressources/documents/document_reference/TableCorrespondanceSirenInsee.zip', '.xlsx', 'brut.communes_siren_2021', 'Banatic_SirenInsee2021.xlsx'],
    ['https://www.banatic.interieur.gouv.fr/V5/ressources/documents/document_reference/TableCorrespondanceSirenInsee.zip', '.xlsx', 'brut.communes_siren_2020', 'Banatic_SirenInsee2020.xlsx'],
    ['https://www.banatic.interieur.gouv.fr/V5/ressources/documents/document_reference/TableCorrespondanceSirenInsee.zip', '.xlsx', 'brut.communes_siren_2019', 'Banatic_SirenInsee2019.xlsx'],
    ['https://www.banatic.interieur.gouv.fr/V5/ressources/documents/document_reference/TableCorrespondanceSirenInsee.zip', '.xls', 'brut.communes_siren_2018', 'Banatic_SirenInsee2018.xls'],
    ['https://www.banatic.interieur.gouv.fr/V5/ressources/documents/document_reference/TableCorrespondanceSirenInsee.zip', '.xls', 'brut.communes_siren_2017', 'Banatic_SirenInsee2017.xls'],
    ['https://www.banatic.interieur.gouv.fr/V5/ressources/documents/document_reference/TableCorrespondanceSirenInsee.zip', '.xls', 'brut.communes_siren_2016', 'Banatic_SirenInsee2016.xls'],
    ['https://www.banatic.interieur.gouv.fr/V5/ressources/documents/document_reference/TableCorrespondanceSirenInsee.zip', '.xls', 'brut.communes_siren_2015', 'Banatic_SirenInsee2015.xls'],
]


def psycopg2_conn():
    """
    Crée une connexion à psycopg2
    Returns: connexion psycopg2
    """    
    conn = psycopg2.connect(
        host=os.environ.get('DB_HOST'),
        port=os.environ.get('DB_PORT'),
        dbname=os.environ.get('DB_NAME'),
        user=os.environ.get('DB_USER'),
        password=os.environ.get('DB_AUTO_PASSWORD')
    )
    return conn


def creation_engine():
    """Cré un engine de connexion sqlalchemy

    Returns:
        sqlalchemy engine
    """    
    # Crée le lien de connexion à la base postgresql
    db_host = os.environ.get('DB_HOST')
    db_port = os.environ.get('DB_PORT')
    db_name = os.environ.get('DB_NAME')
    db_user = os.environ.get('DB_USER')
    db_password = os.environ['DB_AUTO_PASSWORD']

    # Créer la chaîne de connexion à la base de données
    connexion_str = f'postgresql://{db_user}:{db_password}@{db_host}:{db_port}/{db_name}'

    # Établir une connexion à la base de données
    engine = create_engine(connexion_str)
    return engine


def import_stock(url, type):
    """
    Télécharge un fichier pour le mettre dans le répertoire stockage_fichier_temp

    Args:
        url (str): url du fichier
        type (str): extension du fichier
    """    
    r = requests.get(url)
    if r.status_code == 200:
        chemin = path + "/test" + type
        with open(chemin, "xb") as f:
            f.write(r.content)
    else:
        print("Échec du téléchargement du fichier.")


def zip_csv(url):
    """Importe un fichier zip à partir d'une url

    Args:
        url (str): url du fichier zip
    """    
    import_stock(url, '.zip')


def unzip(file, destination):
    """
    extraits tous les fichiers d'un .zip

    Args:
        file (str): chemin vers le fichier zip
        destination (str): chemin vers le répertoire de destination
    """   
    with zipfile.ZipFile(file, 'r') as zip_ref:
        zip_ref.extractall(destination)


def convert_to_csv(type, fichier=None):
    """Converti un fichier en csv, ne fait rien si le fichier est déjà un csv

    Args:
        type (str): extension du fichier
        fichier (str, optional): Nom du fichier s'il est différent de test.type. Defaults to None.
    """    
    if fichier is not None:
        os.rename(path + '/' + fichier, path + '/test' + type)

    if type == '.csv':
        return
    elif type == '.xlsx' or type == '.xls':
        df = pd.DataFrame(pd.read_excel(path + '/test' + type, dtype='str')).to_csv(index=None, header=True)
        with open(path + '/test.csv', "x") as f:
            f.write(df)
        try:
            os.remove(path + '/test' + type)
        except:
            print('erreur lors de la supression du fichier')
    elif type == '.json':
        df = pd.read_json(path + '/test' + type)
        features_df = pd.json_normalize(df['features'])
        df = df.drop(columns=['features'])
        df = pd.concat([df, features_df], axis=1).to_csv(index=None, header=True)

        with open(path + '/test.csv', "x") as f:
            f.write(df)
        try:
            os.remove(path + '/test' + type)
        except:
            print('erreur lors de la supression du fichier')


def import_base(table, fichier=None):
    """Importe un fichier dans la base postgresql (remplace la table par les nouvelles données si elle existe déjà)

    Args:
        table (str): nom de la table dans la base de donnée
        fichier (str, optional): nom du fichier si ce n'est pas test.csv. Defaults to None.
    """    
    if fichier is None:
        file = path + '/test.csv'
    else:
        file = path + '/' + fichier
    f = StringIO(file)
    engine = creation_engine()
    conn = psycopg2_conn()
    cursor = conn.cursor()
    t = table.split('.')
    requete1 = "DELETE FROM " + table + ";"
    requete2 =  f"SELECT column_name FROM information_schema.columns WHERE table_schema = '{t[0]}' AND table_name = '{t[1]}' ;"
    try:
        cursor.execute(requete1)
        conn.commit()
        cursor.execute(requete2)
        infos = cursor.fetchall()
        colonnes = [ x[0] for x in infos]
    except:
        print("la table n'existe pas, création de la nouvelle table")
    cursor.close()
    conn.close()
    try:
        with open(file, "r") as csvfile:
            dialect = csv.Sniffer().sniff(csvfile.read(1024))
            csvfile.seek(0)
            reader = csv.reader(csvfile, dialect)
            premiere_ligne = next(reader)
            nombre_colonnes = len(premiere_ligne)
            df = pd.read_csv(file, sep=dialect.delimiter, quotechar='"', dtype='str', usecols=range(nombre_colonnes))
            try:
                df.columns = colonnes
            except:
                print('')
    except:
        with open(file, "r") as csvfile:
            dialect = csv.Sniffer().sniff(csvfile.read(100))
            csvfile.seek(0)
            reader = csv.reader(csvfile, dialect)
            premiere_ligne = next(reader)
            nombre_colonnes = len(premiere_ligne)
            df = pd.read_csv(file, sep=dialect.delimiter, quotechar='"', dtype='str', usecols=range(nombre_colonnes))
            try:
                df.columns = colonnes
            except:
                print('')
    df.to_sql(t[1], engine, t[0], if_exists="append", index=False, method='multi')


def url_postgre(url, type, table):
    """Importe un fichier dans la base postgresql (remplace la table par les nouvelles données si elle existe déjà)

    Args:
        url (str): url du fichier
        type (str): extension du fichier
        table (str): nom de la table dans la base postgre
    """    
    import_stock(url, type)
    convert_to_csv(type)
    import_base(table)
    files = os.listdir(path)
    for file in files:
        try:
            chemin = os.path.join(path, file)
            os.remove(chemin)
        except:
            print('erreur lors de la supression du fichier (2)')

def url_postgre_zip(url, type, table, fichier):
    """Importe un fichier dans la base postgresql à partir de l'url d'un fichier zip (remplace la table par les nouvelles données si elle existe déjà)

    Args:
        url (str): url du fichier
        type (str): extension du fichier
        table (str): table dans postgresql
        fichier (str): nom du fichier
    """    
    import_stock(url, '.zip')
    unzip(path + '/test.zip',path)
    if table == 'brut.passage_2023':
        df = pd.DataFrame(pd.read_excel(path + '/table_passage_annuelle_2023.xlsx', dtype='str', skiprows=5)).to_csv(index = None, header=True)
        with open(path + '/test.csv', "x") as f:
            f.write(df)
        try:
            os.remove(path + '/table_passage_annuelle_2023.xlsx')
        except:
            print('erreur lors de la supression du fichier')
    else:
        convert_to_csv(type, fichier)

    import_base(table)
    files = os.listdir(path)
    for file in files:
        try:
            chemin = os.path.join(path, file)
            os.remove(chemin)
        except:
            print('erreur lors de la supression du fichier (2)')

def import_docs_final():
    """Importe tous les fichiers dans postgresql
    """    
    for doc in liste_url_types_tables:
        url_postgre(doc[0], doc[1], doc[2])
    for doc in liste_nom_doc_zip:
        url_postgre_zip(doc[0], doc[1], doc[2], doc[3])
