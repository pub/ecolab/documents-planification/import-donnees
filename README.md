## Import documents de planification

## Description
Ce projet a pour but d'importer les différentes données nécessaires au fonctionnement de l'outil de visualisation des documents de planification.

## Schéma brut et import des données

Toutes les tables brut se trouvent dans le tableau ci-dessous (il faut ajouter les tables manquantes si certaines ne sont pas présentes)

[](https://www.notion.so/df9e74cf7ae54936b66f625d5bb3bacf?pvs=21) 

Les tables sont importées dans la base de données grâce au projet import_donnees

Actuellement l’état d’avancement des documents est importé tous les jours (cela sera sûrement modifié à l’avenir pour gagner en frugalité).

Seuls certains référentiels ne sont pas importé quotidiennement : les tables de correspondance siren/codegeo ainsi que la liste des collectivités du banatic, ces tables ont en effet seulement besoin d’être importées une fois par an, lorsque le nouveau millésime sort.

Les tables de l’IGN n’ont pas été importées par ce moyen, le code pour les importer ne semble pas encore opérationnel.

### Import des données :

Un script python permet d’importer les données aussi bien à partir d’url de fichiers qu’à partir de [Flux WFS (projet documents de planification)](https://www.notion.so/Flux-WFS-projet-documents-de-planification-f1dbfe57af2745bdbcf8cb70daa5c06c?pvs=21) 

Il y a deux fichiers principaux dans le projet import-donnees :

- flux_wfs.py : Ce fichier permet d’importer les tables depuis des flux wfs vers la base postgresql
    
    
- import_postgre.py : Ce fichier permet d’importer toutes les tables à dans postgresql à partir d’un lien vers un fichier propre
    
    

Les autres fichiers sont :

- import_banatic.py : importe les fichiers du banatic sur les intercommunalités et les agrèges pour avoir un seul fichier pour toutes les collectivités pour toutes les années
- import_ign.py : importe les fichiers venant de l’ign (le fichier ne sert pas à grand chose et peut être remplacé par une fonction dans main)
    
    
- [main.py](http://main.py) : fonction principale, exécute les fonctions pour importer les tables dans le fichier brut
    
    

### Ajout d’un fichier dans la base de données :

S’il y a un nouveau fichier que vous voulez ajouter dans la base de données

- solution usuelle :
    
    Il faut tout d’abord créer la table correspondante dans postgresql (à la main)
    
    - Si c’est un fichier à partir d’une URL (hors zip), il faut compléter la liste *liste_url_types_tables* du fichier *import_postgre* avec l’url du fichier, l’extension du fichier et le nom de la table.
    - Si c’est un fichier zip, il faut compléter la liste *liste_nom_doc_zip* du fichier *import_postgre* avec l’url du fichier zip, l’extension du fichier à ajouter dans la table, le nom de la table et le nom du fichier à ajouter dans la table.
    - Si c’est un flux WFS
        - s’il vient du géoportail de l’urbanisme, il faut ajouter le nom de la couche et le nom de la table dans la liste *couches_base* du fichier *flux_wfs.py .*
        - S’il vient de l’ign : idem mais avec la liste *couches_base_ign,* ATTENTION ! : LA LIGNE DU MAIN QUI SERT à IMPORTER LES TABLES DE L’IGN EST ACTUELLEMENT COMMENTEE
- Solution beta : idem que la solution usuelle mais il n’y a pas besoin de créer la table correspondante à la main, ne marche que pour les tables à partir d’un url actuellement